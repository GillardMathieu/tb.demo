﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo8
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> mois = new Dictionary<string, int>();

            mois.Add("Jan", 31);
            mois.Add("Feb", 28);
            mois.Add("Mar", 31);
            mois.Add("Apr", 30);
            mois.Add("Mai", 31);
            mois.Add("Jun", 30);
            mois.Add("Jul", 31);
            mois.Add("Aug", 31);
            mois.Add("Sep", 30);
            mois.Add("Oct", 31);
            mois.Add("Nov", 30);
            mois.Add("Dec", 31);

            Console.WriteLine("Ecriver le nom d'un mois");
            string m = Console.ReadLine();

            bool reussi = mois.TryGetValue(m, out int nbJours);

            if (reussi)
            {
                Console.WriteLine($"Le mois demandé compte {nbJours} jours");
            }
            else
            {
                Console.WriteLine("Le mois demandé n'existe pas");
            }

            foreach (KeyValuePair<string,int> kvp in mois)
            {
                Console.WriteLine($"{kvp.Key} : {kvp.Value}");
            }

            Console.ReadKey();


        }
    }
}
