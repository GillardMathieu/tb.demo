﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Exo4
{
    class Program
    {
        private static void AfficherDictionnaire(Dictionary<string, int> dicoAAfficher)
        {
            foreach (KeyValuePair<string, int> kvp in dicoAAfficher)
            {
                Console.WriteLine($"{kvp.Key} : {kvp.Value}");
            }
        }
        private static void AjouterBiere(Dictionary<string, int> monDico)
        {
            Console.WriteLine("Veuillez entrer une nouvelle bière");

            string biere = Console.ReadLine().ToUpper();
            Console.WriteLine($"Quelle quantité de {biere} désirez-vous :");

            int quantite = int.Parse(Console.ReadLine());

            if (!monDico.ContainsKey(biere))
            {
                monDico.Add(biere, quantite);
            }
        }
        private static void ModifierBiere(Dictionary<string, int> monDico)
        {
            Console.WriteLine("Veuillez entrer la bière que vous voulez modifier");
            string reponse = Console.ReadLine().ToUpper();
            while (!monDico.ContainsKey(reponse))
            {
                Console.WriteLine("Cette biere n'existe pas dans notre menu");
                reponse = Console.ReadLine().ToUpper();
            }
            Console.WriteLine("Veuillez entrer la nouvelle quantité");
            int quantite = int.Parse(Console.ReadLine());
            monDico[reponse] = quantite;
        }
        private static void SupprimerBiere(Dictionary<string, int> monDico)
        {
            Console.WriteLine("Veuillez entrer la bière que vous voulez supprimer");
            string reponse = Console.ReadLine().ToUpper();
            while (!monDico.ContainsKey(reponse))
            {
                Console.WriteLine("Cette biere n'existe pas dans notre menu");
                reponse = Console.ReadLine().ToUpper();
            }
            monDico.Remove(reponse);
        }
        static void Main(string[] args)
        {
            // Créer un dictionnaire vide // bar<nom,qté>
            // boucles
            // Demander à l'utilisateur de pouvoir ajouter une valeur clé dans ce dictionnaire
            // Demander à l'utilisateur de spécifier la quantité
            // Afficher l'état du stock

            string fichier = @"C:\aeffacer\Technobel\bieres.csv";

            if (!File.Exists(fichier))
            {
                FileStream f = File.Create(fichier);
                f.Close();
            }

            Dictionary<string, int> monDictionnaire = new Dictionary<string, int>();

            List<string> historique = File.ReadAllLines(fichier).ToList();

            foreach (string item in historique)
            {
                monDictionnaire.Add(item.Split(';')[0], int.Parse(item.Split(';')[1]));
            }

            bool whilebreak = false;

            while (whilebreak == false)
            {
                Console.Clear();

                if (monDictionnaire.Count > 0)
                {
                    Console.WriteLine("Liste des bières : ");
                    AfficherDictionnaire(monDictionnaire);
                }

                Console.WriteLine("Entrer 1 pour ajouter des bières");
                Console.WriteLine("Entrer 2 pour changer la quantité de bière");
                Console.WriteLine("Entrer 3 pour supprimer une bière");
                Console.WriteLine("Entrer 4 pour arrêter le programme");

                int choix = int.Parse(Console.ReadLine());
                switch (choix)
                {
                    case 1:
                        AjouterBiere(monDictionnaire);
                        break;
                        
                    case 2:
                        ModifierBiere(monDictionnaire);
                        break;
                    case 3:
                        SupprimerBiere(monDictionnaire);
                        break;
                    case 4:
                        whilebreak = true;
                        break;
                }                
            }

            List<string> maListeFormatee = new List<string>();

            foreach (KeyValuePair<string, int> kvp in monDictionnaire)
            {
                maListeFormatee.Add($"{kvp.Key};{kvp.Value}"); 
            }

            File.WriteAllLines(fichier, maListeFormatee);
        }
    }
}
