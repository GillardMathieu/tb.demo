﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo4
{
    class Program
    {
        static void Main(string[] args)
        {
            int nb1 = 42;
            int nb2 = 43;

            Console.WriteLine(42 + 43);
            Console.WriteLine(42 - 43);
            Console.WriteLine(42 * 43);
            Console.WriteLine(42 / 43);
            Console.WriteLine(42 % 43);

            Console.WriteLine(nb1++);
            Console.WriteLine(nb1);
            Console.WriteLine(++nb2);

            Console.WriteLine(nb1-=2);
            Console.WriteLine(nb1 *= 2);

            Console.WriteLine("Hello " + "Wordl");

            Console.WriteLine($"Le nombre 1 = {nb1} et le nombre 2 = {nb2}");

            string maChaine = "Hello ";
            maChaine += "world !!!";

            Console.WriteLine(maChaine);

            Console.ReadKey();
        }
    }
}
