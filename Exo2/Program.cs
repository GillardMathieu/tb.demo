﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo2
{
    class Program
    {
        static void Main(string[] args)
        {
            //ex 1
            //Créer un tableau de 10 éléments et demander a l'utilisateur d'entrer 10 éléments
            // afficher la moyenne des entrées de l'utilisateur

            const int NB_ELEMENTS = 10;
            //int[] monTableau = new int[NB_ELEMENTS];

            //for (int i = 0; i < NB_ELEMENTS; i++)
            //{
            //    Console.WriteLine("Veuiller introduire en chiffre entier");

            //    int nbEntier;

            //    while (!int.TryParse(Console.ReadLine(), out nbEntier))
            //    {
            //        Console.WriteLine("Nombre incorrect");
            //    }

            //    monTableau[i] = nbEntier;
            //}

            //double moyenne = monTableau.Average();

            //Console.WriteLine(moyenne);


            ////ex 2
            ////Créer une liste
            ////Demander a l'utilisateur d'entrée des nombres entiers
            ////Lorsque la dernière entrée n'est plus un nombre, calculer la moyenne et l'afficher

            //List<int> maListe = new List<int>();
            //bool maCondition = true;
            //Console.WriteLine("Veuillez entrer des nombres entiers");
            //while (maCondition == true)
            //{
            //    if (int.TryParse(Console.ReadLine(), out int nbEntier))
            //    {
            //        maListe.Add(nbEntier);
            //    }
            //    else
            //    {
            //        maCondition = false;
            //    }
            //}

            //double moyenneDeListe = maListe.Average();

            //Console.WriteLine(moyenneDeListe);

            //ex3
            //Demander a l'utilisateur d'entrer 10 nombres différents
            //Afficher le tableau trier

            int[] monTableauUser = new int[NB_ELEMENTS];
            int cpt = 0;
            while(cpt < NB_ELEMENTS)
            {
                Console.WriteLine("Veuiller introduire en chiffre entier");

                int nbEntier;

                while (!int.TryParse(Console.ReadLine(), out nbEntier))
                {
                    Console.WriteLine("Nombre incorrect");
                }

                if( !monTableauUser.Contains(nbEntier))
                {
                    monTableauUser[cpt] = nbEntier;
                    cpt++;
                }
                else
                {
                    Console.WriteLine("Le nombre existe deja dans le tableau");
                }
            }

            for (int j = 0; j < monTableauUser.Length; j++)
            {
                for (int i = 0; i < monTableauUser.Length; i++)
                {
                    if(i < monTableauUser.Length - 1)
                    {
                        int currentValue = monTableauUser[i];
                        int nextValue = monTableauUser[i + 1];

                        if (currentValue > nextValue)
                        {
                            monTableauUser[i] = nextValue;
                            monTableauUser[i + 1] = currentValue;
                        }
                    }
                }
            }

            Console.WriteLine("Mon tableau trié");
            foreach (int item in monTableauUser)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}
