﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo5
{
    class Program
    {
        private static void AffichageReponse(string nom, string age)
        {
            Console.WriteLine($"Bonjour je m'appelle {nom} et j'ai {age} ans.");
        }

        private static void CalculeMoyenne(List<int> laListe)
        {
            double total = 0;
            foreach (int item in laListe)
            {
                total += item;
            }

            Console.WriteLine($"La moyenne de votre liste est {total / laListe.Count}");
        }
        private static void AfficherListeDouble(List<int> laListe)
        {
            foreach (int item in laListe)
            {
                Console.WriteLine(item*2);
            }
        }
        private static void AfficherListeCarre(List<int> laListe)
        {
            for (int i = 0; i < laListe.Count; i++)
            {
                laListe[i] = laListe[i] * laListe[i];
                Console.WriteLine(laListe[i]);
            }
        }

        private static int CalculerSuiteFibonacci(int indexeDeLaSuite)
        {
            int cpt = 0;
            int current = 0;
            int old = 1;
            while (cpt < indexeDeLaSuite)
            {
                int temp = current;
                current = old;
                old = temp + old;
                cpt++;
            }

            return current;
        }

        private static int CalculerSuiteFibonacciRec(int indexeDeLaSuite, int index = 0, int current = 0, int next = 1)
        {
            int temp = current;
            current = next;
            next = temp + next;
            index++;

            if(index == indexeDeLaSuite)
            {
                return current;
            }
            else
            {
                return CalculerSuiteFibonacciRec(indexeDeLaSuite, index, current, next);
            }
        }

        //


        static void Main(string[] args)
        {
            #region 1.1
            //Console.WriteLine("Veuillez entrer votre nom");
            //string nom = Console.ReadLine();

            //Console.WriteLine("Veuillez entrer votre age");
            //string age = Console.ReadLine();

            //AffichageReponse(nom, age); 
            #endregion

            #region 1.2
            //List<int> maliste = new List<int>() { 15, 12, 16, 17, 3, 5 };
            //CalculeMoyenne(maliste); 
            #endregion

            #region 1.3
            //List<int> maliste = new List<int>() { 15, 12, 16, 17, 3, 5 };
            //Console.WriteLine("Ma Liste doublée");
            //AfficherListeDouble(maliste);
            //Console.WriteLine("Ma Liste normale");
            //foreach (int item in maliste)
            //{
            //    Console.WriteLine(item);
            //} 
            #endregion

            #region 1.4
            //List<int> maliste = new List<int>() { 15, 12, 16, 17, 3, 5 };
            //Console.WriteLine("Ma Liste normale");
            //foreach (int item in maliste)
            //{
            //    Console.WriteLine(item);
            //}
            //Console.WriteLine("Ma Liste au carré");
            //AfficherListeCarre(maliste);
            //Console.WriteLine("Ma Liste après la fonction");
            //foreach (int item in maliste)
            //{
            //    Console.WriteLine(item);
            //} 
            #endregion

            int nb = 0;

            Console.WriteLine("Veuillez entrer un nombre pour l'index");

            nb = CalculerSuiteFibonacciRec(int.Parse(Console.ReadLine()));

            Console.WriteLine(nb);

            Console.ReadKey();
        }
    }
}
