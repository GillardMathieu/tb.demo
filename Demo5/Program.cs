﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo5
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 16;
            if(age >= 18)
            {
                Console.WriteLine("Vous êtes majeur");
            }
            else
            {
                Console.WriteLine("Vous êtes mineur");
            }

            Console.WriteLine(age>=18 ? "Vous êtes majeur" : "Vous êtes mineur");

            int prix = 20;

            int? promotion = null;

            //coalesce
            Console.WriteLine(prix - (prix * (promotion ?? 0) / 100));

            //null safe operator

            string maChaine = null;

            Console.WriteLine(maChaine?.ToLower());

            Console.ReadKey();
        }
    }
}
