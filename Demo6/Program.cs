﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo6
{
    class Program
    {
        static void Main(string[] args)
        {
            var maVariable = 3.14;

            Console.WriteLine(maVariable.GetType());

            if(maVariable is double)
            {
                Console.WriteLine(maVariable*3);
            }
            else
            {
                Console.WriteLine("Opération impossible");
            }
            Console.ReadKey();
        }
    }
}
