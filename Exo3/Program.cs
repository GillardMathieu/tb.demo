﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace Exo3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Créer un fichier qui n'existe pas utilisateur.txt
            //Afficher la liste des utilisateurs
            //Permettre depuis la console d'enregistré la console des users

            //lorsqu'on redémarre le programme, afficher les utilisateurs

            string fichier = @"C:\aeffacer\Technobel\users.txt";

            if(!File.Exists(fichier))
            {
                FileStream f = File.Create(fichier);
                f.Close();
            }

            List<string> users = File.ReadAllLines(fichier).ToList();

            bool chaineVide = false;

            while(chaineVide == false)
            {
                Console.Clear();

                Console.WriteLine("Liste des users : ");
                foreach (string item in users)
                {
                    Console.WriteLine(item);
                }

                Console.WriteLine("Veuillez entrer un nouvel utilisateur. Enter pour arrêter");

                string entree = Console.ReadLine();
                if (entree != string.Empty)
                {
                    users.Add(entree);
                }
                else
                {
                    chaineVide = true;
                }
                
            }

            File.WriteAllLines(fichier, users);
        }
    }
}
