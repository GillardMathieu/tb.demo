﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Veuillez entrer un nombre de seconde");

            int reponseEnSeconde = int.Parse(Console.ReadLine());

            int heures = reponseEnSeconde / 3600;

            int minutes = (reponseEnSeconde % 3600) / 60;

            int secondes = reponseEnSeconde % 60;

            Console.WriteLine($"{heures} heure(s) {minutes} minute(s) {secondes} seconde(s)");

            Console.ReadKey();
        }
    }
}
